FROM ruby:2.6.3-slim

RUN apt-get update -qq \
    && apt-get install -y --no-install-recommends \
    build-essential \
    libpq-dev \
    nodejs \
    wget  \
    postgresql-client \
    && rm -rf /var/lib/apt/lists/*t

RUN mkdir /mnt/rails/
COPY start-dev.sh /start-dev.sh
RUN chmod +x /start-dev.sh
COPY . /mnt/rails
COPY Gemfile Gemfile.lock /mnt/rails/

WORKDIR /mnt/rails/
# Bundle install
RUN gem install bundler:1.17.3
RUN bundler install --path vendor/bundle
CMD ["./start-dev.sh"]

EXPOSE 3000
