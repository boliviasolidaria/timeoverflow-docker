
###### tags: `docker` `bolivia-solidaria` `timeoverflow`
# Docker para el proyecto timeoverflow

Para tener una mejor organizacion en el grupo de **Bolivia Solidaria**, se necesita montar un banco de tiempo, **TimeOverFLow** nos ayuda a centralizar los perfiles de los voluntarios y también los contactos de las organizaciones.  

[https://github.com/coopdevs/timeoverflow](timeoverflow)

Este proyecto usa las siguientes tecnologias:

* Ruby 2.6.3
* Ruby on Rails bundler:1.17.3
* Postgres 9.4
* Redis
* Elastic Seach 0.9


## Levantar el entorno de desarrollo desde una imagen pre-construida

### Requisitos
1. git
1. docker [https://get.docker.com/](Instalación)

### Pasos

#### 1.- Creacion de la red

Recuerda crear la red para que los contenedores se comuniquen por el nombre del contenedor
```shell=
docker network create timeoverflow-net
```

Reiniciamos docker

```shell=
 systemctl restart docker
```
Ahora se debe levantar los demas contenedores
#### 2.- Base de datos
```shell=
 docker run -d \
    --name postgres-timeoverflow \
    --network=timeoverflow-net \
    -e POSTGRES_PASSWORD=mysecretpassword \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v data-base:/var/lib/postgresql/data \
    postgres:9.4
```

#### 3.- Elastic Search

```shell=
 docker run -d \
   --name elasticsearch-timeoverflow \
   --network=timeoverflow-net \
   -e "discovery.type=single-node" \
   -p 9200:9200 \
   tianjiang/elasticsearch-0.9
```

#### 4.- Redis

```shell=
docker run \
--name=redis-timeoverflow \
--network=timeoverflow-net \
-p 6379:6379 \
-d redis:alpine
```

#### 5.- Comprobando resultados

Comprobando que todo este corriendo con el siguiente comando:
```shell=
docker ps
```
Se debe tener un resultado similar, de lo contrario repite los pasos:
```shell=
root@instance-1:/home/starsaminf/timeoverflow-docker/rails# docker ps
CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS              PORTS                              NAMES
7ee0caabe79a        redis:alpine                  "docker-entrypoint.s…"   20 minutes ago      Up 20 minutes       0.0.0.0:6379->6379/tcp             redis-timeoverflow
a975c236082c        tianjiang/elasticsearch-0.9   "/elasticsearch/bin/…"   20 minutes ago      Up 20 minutes       0.0.0.0:9200->9200/tcp, 9300/tcp   elasticsearch-timeoverflow
1f7bc3205ea3        postgres:9.4                  "docker-entrypoint.s…"   20 minutes ago      Up 20 minutes       5432/tcp                           postgres-timeoverflow
```
#### 6.- Proyecto

```shell=
git clone https://gitlab.com/boliviasolidaria/timeoverflow-docker/
cd timeoverflow-docker
```
Primero se inicializa la base de datos
```shell=
docker run \
 --rm \
 --network timeoverflow-net \
 -it \
 -v "$(pwd)/database.yml:/mnt/rails/config/database.yml" \
 -v "$(pwd)/.env:/mnt/rails/.env" \
 registry.gitlab.com/boliviasolidaria/timeoverflow-docker:latest /bin/bash -c "bundle exec rake db:setup db:seed"
```
Luego se levanta la aplicacion web
```shell=
docker run -d \
 --name=web-timeoverflow \
 --network timeoverflow-net \
 -p 80:3000 \
 -v "$(pwd)/database.yml:/mnt/rails/config/database.yml" \
 -v "$(pwd)/.env:/mnt/rails/.env" \
 registry.gitlab.com/boliviasolidaria/timeoverflow-docker:latest
```
## Listo!
Entra al navegador e ingresa a:
```shell=
localhost
```
## Credenciales

**Admin:**
username: admin@timeoverflow.org
password: 1234test

**User:**
username: user@timeoverflow.org
password: 1234test

----

Eso es todo :), si quieres construir tu propia imagen desde cero continua con la lectura.


## Levantar el entorno de desarrollo desde cero
### Requisitos
1. git
1. docker [https://get.docker.com/](Instalación)
1. docker-compose 3 [https://docs.docker.com/compose/install/](Instalación)

### Pasos

```shell=
git clone https://gitlab.com/boliviasolidaria/timeoverflow-docker/
cd timeoverflow-docker
chmod +x run.sh
./run.sh
```
### Listo

Entra al navegador e ingresa a:
```shell=
localhost
```
Lo unico malo de este es que toma mas tiempo levantar el entorno.


##### happyCoding


### Capturas
![1](https://codimd.s3.shivering-isles.com/demo/uploads/upload_90094b4d207047b1ed989890d0a9e259.png)

![2](https://codimd.s3.shivering-isles.com/demo/uploads/upload_4a4e8dffa172065da78dc721bc7940d8.png)

![3](https://codimd.s3.shivering-isles.com/demo/uploads/upload_3d5d770506f68163a37bbc10f6d6e7c9.png)


### Notas
Crear usuario en postgres con privilegios

```shell=
CREATE USER overflow WITH PASSWORD 'miclave';
CREATE database overflow;
grant all privileges on database overflow to overflow;
Alter USER overflow WITH SUPERUSER;
```

##### Como construir tu propia imagen ?

coming soon ...

`docker build --rm=false -t my-image .`
