#!/bin/bash

FILE=rails
if [ ! -d "$FILE" ]; then
    git clone https://github.com/coopdevs/timeoverflow
    mv timeoverflow rails
fi

cp -f .env rails/.env
cp -f Dockerfile rails/Dockerfile
cp -f database.yml rails/config/database.yml
cp -f start-dev.sh rails/start-dev.sh
docker-compose up